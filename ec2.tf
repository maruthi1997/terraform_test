provider "aws" {
    region = "us-east-1"
    profile = "default"
}

resource "aws_instance" "myec2"{
    ami = "ami-02eac2c0129f6376b"
    instance_type = "t2.micro"
    subnet_id = "subnet-014ebc20"
    key_name = "maruthi"
    security_groups = ["sg-0d34178918aa17836"]
    tags = {
        Name = "jenkins-terra"
    }
}